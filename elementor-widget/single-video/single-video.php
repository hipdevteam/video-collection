<?php

namespace HipVideoCollection;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}


class ElWidget extends Widget_Base
{

	public function get_name() // phpcs:ignore
	{
		return 'single-video';
	}

	public function get_title() // phpcs:ignore
	{
		return 'Video from Collection';
	}

	public function get_icon() // phpcs:ignore
	{
		return 'eicon-play';
	}

	public function get_categories() // phpcs:ignore
	{
		return ['hip'];
	}

	protected function _register_controls() // phpcs:ignore
	{

		$this->start_controls_section(
			'section_content',
			[
			'label' => 'Global Settings',
			]
		);

		$this->add_control(
			'video_id',
			[
			'label' => 'Video',
			'type' => \Elementor\Controls_Manager::SELECT,
			'default' => array_key_first(\HipVideoCollection\VideoCollection::getOptions()),
			'options' => \HipVideoCollection\VideoCollection::getOptions()
			]
		);

		$this->add_control(
			'show_title',
			[
			'label' => 'Title Position',
			'type' => \Elementor\Controls_Manager::SELECT,
			'default' => 'bottom',
			'options' => [
			 'bottom' => 'Bottom',
			 'top' => 'Top',
			 'hide' => 'Hide'
			]
			]
		);

		$this->add_control(
			'show_description',
			[
			'label' => 'Show Description',
			'type' => \Elementor\Controls_Manager::SELECT,
			'default' => 'false',
			'options'	=> [
			'true'		=> 'True',
			'false'		=> 'False'
			]
			]
		);

		$this->end_controls_section();
	}

	protected function render()
	{
		$settings = $this->get_settings_for_display();

		$show_title = '';
		$title_pos = '';
		if ($settings['show_title'] !== 'hide') {
			if ($settings['show_title'] == 'bottom') {
				$title_pos = ' title_pos="bottom"';
			} else {
				$title_pos = ' title_pos="top"';
			}
		} else {
			$show_title = ' show_title=0';
		}

		$show_description = ' show_description=' . $settings['show_description'];

		echo do_shortcode('[hip_video id="' . $settings['video_id'] . '"' . $show_title . $title_pos . $show_description . ']');
	}
}
