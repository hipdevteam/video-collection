<?php

namespace HipVideoCollection;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

class ElVideoGalleryWidget extends Widget_Base
{

	public function get_name() // phpcs:ignore
	{
		return 'video-gallery';
	}

	public function get_title() // phpcs:ignore
	{
		return 'Video Gallery from Collection';
	}

	public function get_icon() // phpcs:ignore
	{
		return 'eicon-video-playlist';
	}

	public function get_categories() // phpcs:ignore
	{
		return ['hip'];
	}

	public static function getOptions()
	{
		$options = ['all' => 'All Videos'];
		$terms = get_terms(array('taxonomy' => 'video-category'));
		foreach ($terms as $term) {
			$options[$term->slug] = $term->name;
		}

		return $options;
	}

	protected function _register_controls() // phpcs:ignore
	{

		$this->start_controls_section(
			'section_content',
			[
				'label' => 'Gallery Settings',
			]
		);

		$this->add_control(
			'video_category',
			[
				'label' => 'Category',
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => '',
				'options' => \HipVideoCollection\ElVideoGalleryWidget::getOptions(),
				'default' => 'all'
			]
		);

		$this->add_control(
			'video_per_page',
			[
				'label' => 'Number of Videos',
				'type' => \Elementor\Controls_Manager::TEXT,
				'description' => __('<br>Number of total videos is ' . \HipVideoCollection\VideoCollection::totalVideos()),
				'title' => 'Leave empty to select all'
			]
		);

		$this->add_control(
			'video_order_by',
			[
				'label' => 'Order By',
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'menu_order' => 'Menu Order',
					'date'       => 'Date',
					'ID'         => 'ID'
				]
			]
		);

		$this->add_control(
			'video_order',
			[
				'label' => 'Order',
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'DESC' => 'DESC',
					'ASC'  => 'ASC'
				]
			]
		);

		$this->end_controls_section();
	}

	protected function render()
	{
		$settings = $this->get_settings_for_display();

		$args = array();
		$args['post_type'] = 'hip_video_collection';

		if ($settings['video_category'] != 'all') {
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'video-category',
					'field' => 'slug',
					'terms' => [ $settings['video_category'] ],
					'operator' => 'IN'
				)
			);
		}
		if (!empty($settings['video_per_page'])) {
			$args['posts_per_page'] = $settings['video_per_page'];
		} else {
			$args['posts_per_page'] = '-1';
		}
		$args['paged'] = ( \get_query_var('page') ) ? \get_query_var('page') : 1;
		$args['order'] = $settings['video_order'];
		$args['orderby'] = $settings['video_order_by'];
		$query = new \WP_Query((object)$args);
		?>
			<div class="video-gallery-wrapper">
				<?php if ($query->have_posts()) : ?>
					 <div class="video-container">
						<?php while ($query->have_posts()) :
							$query->the_post(); ?>
							<div class="video-single-item">
								<?php
									$video = Video::getVideo(get_the_ID());
									echo $video->getFrontend();
								?>
							</div>
						<?php endwhile; ?>
					</div>
					<div class="fl-builder-pagination">
						<ul>
							<li>
								<?php echo paginate_links(array('total' => $query->max_num_pages)); ?>
							</li>
						</ul>
					</div>
					<div class="load-more-btn">
						<a href="#" class="button-primary">Load More</a>
					</div>
				<?php endif; ?>
			</div>
			<?php
				wp_reset_postdata();
				wp_enqueue_script('elementor-video-gallery-frontend', HVC_URL . '/elementor-widget/video-gallery/js/frontend.js', ['jquery'], HVC_VERSION);
				wp_enqueue_style('elementor-video-gallery-frontend', HVC_URL . '/elementor-widget/video-gallery/css/frontend.css', [], HVC_VERSION);
	}
}
