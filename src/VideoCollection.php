<?php

namespace HipVideoCollection;

class VideoCollection
{
	/**
	 * custom post type name
	 * @var string
	 * custom post type name
	 */

	public $post_type = 'hip_video_collection';

	/**
	 * assets directory
	 * @var string
	 */
	public $assets;
	public static $all_posts;

	public $video_collection_settings;

	public static function totalVideos()
	{
		return count(VideoCollection::$all_posts);
	}

	public static function getOptions()
	{
		$posts = VideoCollection::$all_posts;
		$options = [];
		foreach ($posts as $post) {
			$options[$post->ID] = $post->post_title;
		}

		return $options;
	}
	/**
	 * method __construct()
	 * set values in properties
	 * @uses add_action(), add_shortcode() wp functions
	 */
	public function __construct()
	{
		$this->assets = HVC_URL . '/assets/';
		$this->video_collection_settings = Settings::getSettings();
		self::$all_posts = get_posts(['numberposts' => -1, 'post_type' => 'hip_video_collection']);
		add_action('init', [$this, 'registerPostType']);
		add_action('init', [$this, 'updatePlugin']);
		add_action('add_meta_boxes', [$this, 'registerMetaBox'], 10, 2);
		add_action('admin_enqueue_scripts', [$this, 'enqueueAssets']);
		add_action('wp_enqueue_scripts', [$this, 'enqueueFrontendAssets']);
		add_action('save_post', [$this, 'savePostMeta'], 10, 3);
		add_action('rest_api_init', [$this, 'videoCollectionRestAPIInit']);
		add_action('wp_head', [$this, 'renderCss']);
		add_filter('archive_template', [$this, 'archiveTemplates']);
		add_shortcode('hip_video', [$this, 'renderShortcode']);
		add_action('single_template', [$this, 'singleVideoTemplate']);
		add_action('init', [$this, 'loadModules']);
		add_action('media_buttons', [$this, 'addVideoButton']);
		add_action('admin_footer', [$this, 'addVideoPopupContent']);
		add_action('rest_api_init', [$this, 'addMetasInRestApi']);

		$this->update_process = new Background\AddPadding();
	}

	public function registerPostType()
	{
		register_post_type($this->post_type, [
			'labels' => [
				'name' => $this->video_collection_settings['video_archive_title'],
				'singular_name' => 'Video',
				'all_items' => 'All Videos',
				'add_new' => 'New Video',
				'add_new_item' => 'Add New Video'
			],
			'public' => true,
			'has_archive' => $this->video_collection_settings['video_archive_slug'],
			'rewrite' => ['slug' => $this->video_collection_settings['video_archive_slug']],
			'supports' => ['title', 'editor'],
			'show_in_rest' => true,
		]);

		register_taxonomy('video-category', [$this->post_type], [
			'labels' => [
				'name' => 'Video Categories',
				'singular_name' => 'Video Category',
				'all_items' => 'All Video Categories',
				'add_new' => 'New Video Category',
				'add_new_item' => 'Add New Video Category'
			],
			'rewrite' => ['slug' => 'video-category'],
			'public' => true,
			'show_admin_column' => true,
			'hierarchical' => true,
			'show_in_rest' => true
		]);
	}

	public function singleVideoTemplate($template)
	{
		global $post;
		if (get_post_type($post) == $this->post_type && is_single()) {
			$template = HVC_PATH . "/templates/single-hip_video_collection.php";
		}
		return $template;
	}

	public function archiveTemplates($template)
	{
		global $post;
		if (is_post_type_archive($this->post_type) || is_tax('video-category')) {
			$template = HVC_PATH . '/templates/archive.php';
		}
		return $template;
	}

	public function renderShortcode($atts)
	{
		$params = shortcode_atts([
			'title' => '',
			'id' => 0,
			'show_title' => 1,
			'title_pos' => 'bottom',
			'show_description' => false
		], $atts);

		if ($params['show_description'] == 'false') {
			$params['show_description'] = false;
		}

		if ($params['title']) {
			$params['title'] = trim($params['title']);
			$post = get_page_by_title($params['title'], OBJECT, $this->post_type);
			$params['id'] = $post->ID;
		}

		$video = Video::getVideo($params['id']);

		if ($video) {
			ob_start();
			$video->getFrontend($params['show_title'], $params['title_pos'], $params['show_description']);

			return ob_get_clean();
		}

		return false;
	}

	public function registerMetaBox($butterbean, $post_type)
	{
		add_meta_box(
			'pvc-video-collection',
			'Video Settings',
			[$this, 'renderMetaBox'],
			$this->post_type,
			'normal',
			'high'
		);
	}

	public function renderMetaBox($post)
	{
		wp_nonce_field(basename(__FILE__), 'pvc_box_nonce');
		$video_meta = get_post_meta($post->ID);
		?>
		<div>
			<label for="_pvc_display_title">Display Title</label>
			<span class="description">Title to appear under video preview (if different than post title)</span>
			<input name="_pvc_display_title" type="text" class="widefat"
				   value="<?php echo !empty($video_meta['_pvc_display_title'][0]) ? $video_meta['_pvc_display_title'][0] : ''; ?>"/>
			<br>
			<label for="_pvc_video_embed">Embed URL</label>
			<span class="description">Must be a youtube share url</span>
			<input name="_pvc_video_embed" id="_pvc_video_embed" type="url" class="widefat"
				   value="<?php echo !empty($video_meta['_pvc_video_embed'][0]) ? $video_meta['_pvc_video_embed'][0] : ''; ?>"/>
			<span class="validation-txt"></span>
			<label for="_pvc_screenshot">Screenshot</label>
			<span class="description" style="margin-bottom: 5px; display: block">If none selected, the default screenshot generated by youtube will be used.</span>
			<input name="_pvc_screenshot" type="hidden"
				   value="<?php echo !empty($video_meta['_pvc_screenshot'][0]) ? $video_meta['_pvc_screenshot'][0] : ''; ?>"/>
			<input name="_pvc_default_screenshot" type="hidden"
				   value="<?php echo !empty($video_meta['_pvc_default_screenshot'][0]) ? $video_meta['_pvc_default_screenshot'][0] : '' ?>"/>
			<input name="_hvc_video_responsive_padding" type="hidden"
				   value="<?php echo !empty($video_meta['_hvc_video_responsive_padding'][0]) ? $video_meta['_hvc_video_responsive_padding'][0] : '' ?>">
			<?php if (empty($video_meta['_pvc_screenshot'][0] || $video_meta['_pvc_default_screenshot'][0])) : ?>
				<div class="pvc_image_placeholder">No image selected</div>
				<button class="button pvc_image_button" name="pvc_upload_image">Upload</button>
			<?php elseif ($video_meta['_pvc_screenshot'][0]) : ?>
				<?php echo wp_get_attachment_image($video_meta['_pvc_screenshot'][0], 'medium', false, ['class' => 'pvc_admin_image']); ?>
				<button class="button pvc_image_button" name="pvc_remove_image">Remove</button>
				<button class="button pvc_image_button" name="pvc_replace_image">Replace</button>
			<?php elseif ($video_meta['_pvc_default_screenshot'][0]) : ?>
				<img src="<?php echo $video_meta['_pvc_default_screenshot'][0] ?>" class="pvc_admin_image">
				<button class="button pvc_image_button" name="pvc_remove_image">Remove</button>
				<button class="button pvc_image_button" name="pvc_replace_image">Replace</button>
			<?php endif; ?>
		</div>
		<?php
	}

	public function savePostMeta($post_id, $post, $update)
	{
		$title = '';
		$embed = '';
		$screenshot = '';
		$default_screenshot = '';
		$responsive_padding = '';

		if (!isset($_POST["pvc_box_nonce"]) || !wp_verify_nonce($_POST["pvc_box_nonce"], basename(__FILE__))) {
			return $post_id;
		}

		if (!current_user_can("edit_post", $post_id)) {
			return $post_id;
		}

		if (defined("DOING_AUTOSAVE") && DOING_AUTOSAVE) {
			return $post_id;
		}

		if ($this->post_type != $post->post_type) {
			return $post_id;
		}
		if (isset($_POST['_pvc_display_title'])) {
			$title = sanitize_text_field($_POST['_pvc_display_title']);
		}

		update_post_meta($post_id, '_pvc_display_title', $title);

		if (isset($_POST['_pvc_video_embed'])) {
			$embed = esc_url_raw(trim($_POST['_pvc_video_embed']));
			if (get_post_status($post_id) != 'draft') {
				$validURL = Video::validateURL($embed);
				if (!$validURL) {
					$error = new \WP_Error('pvc_invalid_embed', 'The video embed must be from YouTube.');
					wp_die($error->get_error_message());
				}
			}
		}
		update_post_meta($post_id, '_pvc_video_embed', $embed);

		if (isset($_POST['_pvc_screenshot'])) {
			$screenshot = absint($_POST['_pvc_screenshot']);
		}
		update_post_meta($post_id, '_pvc_screenshot', $screenshot);

		if (isset($_POST['_pvc_video_embed'])) {
			$video = new Video(['embed' => $_POST['_pvc_video_embed']]);
			$default_screenshot = $video->getDefaultScreenshot();
			$responsive_padding = $video->getVideoPadding();
		}
		update_post_meta($post_id, '_pvc_default_screenshot', $default_screenshot);
		update_post_meta($post_id, '_hvc_video_responsive_padding', $responsive_padding);
		return $post_id;
	}

	public function enqueueAssets()
	{
		global $post;
		if (get_post_type($post) == $this->post_type) {
			wp_enqueue_media();
			wp_enqueue_style('pvc-admin-style', HVC_URL . '/assets/admin.css', [], HVC_VERSION);
			wp_enqueue_script('pvc-admin-script', HVC_URL . '/assets/admin.js', [], HVC_VERSION, true);
		}
		wp_enqueue_style('hvc-admin-popup-style', HVC_URL . '/assets/admin-popup.css', [], HVC_VERSION);
		wp_enqueue_script('hvc-admin-popup-script', HVC_URL . '/assets/admin-popup.js', ['jquery'], HVC_VERSION, true);
		add_thickbox();
	}

	public function enqueueFrontendAssets()
	{
		wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css');
		wp_enqueue_style('pvc-frontend', HVC_URL . '/assets/frontend.css', [], HVC_VERSION);
		wp_enqueue_script('pvc-popup-script', HVC_URL . '/assets/frontend.js', ['jquery'], HVC_VERSION, true);
	}

	/**
	 * Load beaver builder modules and elementor widgets
	 * @return void
	 */

	public function loadModules()
	{
		if (class_exists('FLBuilder')) {
			require_once(HVC_PATH . '/bb-module/single-video/single-video.php');
			require_once(HVC_PATH . '/bb-module/video-gallery/video-gallery.php');
		}
		add_action('elementor/elements/categories_registered', [$this, 'addElementorWidgetCategories']);
		add_action('elementor/widgets/widgets_registered', [$this, 'registerWidgets'], 99);
		add_action('elementor/frontend/after_enqueue_styles', [$this, 'widgetStyles']);
		add_action('elementor/frontend/after_register_scripts', [$this, 'widgetScripts']);
	}

	public function widgetStyles()
	{
		wp_register_style('elementor-video-gallery-frontend', HVC_URL . '/elementor-widget/video-gallery/css/frontend.css', [], HVC_VERSION);
	}

	public function widgetScripts()
	{
		wp_register_script('elementor-video-gallery-frontend', HVC_URL . '/elementor-widget/video-gallery/js/frontend.js', [], HVC_VERSION);
	}

	public function addElementorWidgetCategories($elements_manager)
	{
		$elements_manager->add_category(
			'hip',
			[
				'title' => 'hip',
				'icon' => 'fa fa-video',
			]
		);
	}

	private function includeWidgetsFiles()
	{
		require_once(HVC_PATH . '/elementor-widget/single-video/single-video.php');
		require_once(HVC_PATH . '/elementor-widget/video-gallery/video-gallery.php');
	}

	public function registerWidgets()
	{

		$this->includeWidgetsFiles();

		\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new ElWidget());
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new ElVideoGalleryWidget());
	}

	/**
	 * initialize videoCollectionRestAPIInit()
	 * @uses \HipVideoCollection\VideoCollectionSettingsAPI class
	 * @string
	 */
	public function videoCollectionRestAPIInit()
	{
		$api = new SettingsAPI();
		$api->addRoutes();
	}

	/**
	 * render css for frontend
	 * @uses video_collection_settings_renderCss() method
	 * @return mixed
	 */
	public function renderCss()
	{
		ob_start();
		?>

		<style id="hip-video-collection-css" type="text/css">
			<?php echo $this->buildCssVideoCollectionSettings();?>
		</style>
		<?php

		return ob_get_flush();
	}

	/**
	 * prepare css according to video collection settings
	 * @uses buildCssVideoCollectionSettings() method
	 * @return string
	 */
	private function buildCssVideoCollectionSettings()
	{
		$general_settings = array();

		if (class_exists('Hip\Theme\Settings\General')) {
			$general_settings = \Hip\Theme\Settings\General\Settings::getSettings();
			if (empty($general_settings['primary_color'])) {
				$general_settings['primary_color'] = '#00b0d8';
			}
			if (empty($general_settings['secondary_color'])) {
				$general_settings['secondary_color'] = '#17758b';
			}
		} else {
			$general_settings['primary_color'] = '#00b0d8';
			$general_settings['secondary_color'] = '#17758b';
		}

		$css = '';

		if (!empty($this->video_collection_settings['video_play_btn_color'])) {
			$css .= '.pps-video-preview-wrap a .push-btn i{ color: ' . $this->video_collection_settings['video_play_btn_color'] . ';}';
		} else {
			$css .= '.pps-video-preview-wrap a .push-btn i{ color: ' . $general_settings['primary_color'] . ';}';
		}
		if (!empty($this->video_collection_settings['video_play_btn_hover_color'])) {
			$css .= ' .pps-video-preview-wrap a:hover .push-btn i{ color: ' . $this->video_collection_settings['video_play_btn_hover_color'] . ';}';
		} else {
			$css .= '.pps-video-preview-wrap a:hover .push-btn i{ color: ' . $general_settings['secondary_color'] . ';}';
		}
		if (!empty($this->video_collection_settings['video_play_btn_bg_color'])) {
			$css .= '.pps-video-preview-wrap .push-btn{ background-color: ' . $this->video_collection_settings['video_play_btn_bg_color'] . ';}';
		} else {
			$css .= '.pps-video-preview-wrap .push-btn{ background-color: #fff;}';
		}
		if (!empty($this->video_collection_settings['video_play_btn_hover_bg_color'])) {
			$css .= ' .pps-video-preview-wrap a:hover span.push-btn { background-color:' . $this->video_collection_settings['video_play_btn_hover_bg_color'] . ';
            }';
		}
		if (empty($this->video_collection_settings['play_defalut_style'])) {
			$css .= ' .pps-video-preview-wrap .push-btn { opacity: 1; box-shadow: none; animation: none;
            }';
		}
		if (!empty($this->video_collection_settings['embed_max_width'])) {
			$css .= '.vid_popup_cont{width:' . $this->video_collection_settings['embed_max_width'] . 'px;}';
		} else {
			$css .= '.vid_popup_cont{width:900px;}';
		}
		return $css;
	}

	public function updatePlugin()
	{
		$version = get_option('hvc_version');
		if (!$version) {
			$version = "1.5.5";
		}

		if (version_compare($version, "1.5.5", '<=')) {
			$videos = $this->getAllVideos();
			foreach ($videos as $video) {
				$this->update_process->push_to_queue($video);
			}
			$this->update_process->save()->dispatch();
			update_option('hvc_version', HVC_VERSION);
		}
	}

	/*
	 * add a 'add-video' button on the top of the editor
	 * @return void
	 */
	public function addVideoButton()
	{
		ob_start();
		?>
		<a href="#TB_inline?height=350&inlineId=insert-video-popup" class="button thickbox wp_doin_media_link"
		   id="add_video_shortcode" title="Add video from video collection"><span class="wp-media-buttons-icon dashicons dashicons-format-video"></span> Add video</a></li>
		<?php
		return ob_get_flush();
	}

	/*
	 * popup content for add video
	 * @uses getAllVideos method
	 * @return void
	 */
	public function addVideoPopupContent()
	{
		ob_start();
		?>
		<div id="insert-video-popup" style="display: none">
			<div class="insert-video-popup-cont">
				<h3>Select video to insert</h3>
				<hr>
				<table class="insert-video-form">
					<tr>
						<td width="150"><label for="video_name">Video</label></td>
						<td>
							<select name="video_name" id="video_name">
								<option value="0" disabled selected>Select video from below...</option>
								<?php foreach ($this->getAllVideos() as $video) : ?>
									<option value="<?php echo $video->id; ?>"><?php echo $video->title; ?></option>
								<?php endforeach; ?>
							</select>
						</td>
					</tr>
					<tr>
						<td><label for="show_title">Display Title</label></td>
						<td><input type="checkbox" name="show_title" id="show_title" value="1">Yes</td>
					</tr>
					<tr class="hidden">
						<td><label for="title_pos">Title position</label></td>
						<td>
							<select name="title_pos" id="title_pos">
								<option value="bottom">Bottom</option>
								<option value="top">Top</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<hr>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<button type="button" class="button-primary insert-video-shortcode">Insert Shortcode
							</button> &nbsp;&nbsp;&nbsp;
							<a class="button" href="#" onclick="tb_remove();
							return false;">Cancel</a>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php
		return ob_get_flush();
	}

	/*
	 * get all published videos
	 */
	public function getAllVideos()
	{

		$videos = [];
		foreach (self::$all_posts as $post) {
			$videos[] = Video::getVideo($post->ID);
		}

		return $videos;
	}

	/**
	 * add video meta fields in rest api
	 * @return void
	 */

	public function addMetasInRestApi()
	{

		register_rest_field($this->post_type, 'video_meta', array(
				'get_callback' => array($this, 'getMetaForRestApi'),
				'schema' => null,
			));
	}

	/**
	 * get video meta fields for rest api
	 * @param object
	 * @return array
	 */

	public function getMetaForRestApi($object)
	{
		$metas = get_post_meta($object['id']);

		return array(
			'display_title' => $metas['_pvc_display_title'][0],
			'video_embed' => $metas['_pvc_video_embed'][0],
			'screenshot' => !empty($metas['_pvc_screenshot'][0]) ? wp_get_attachment_image_src($metas['_pvc_screenshot'][0], 'large')[0] : $metas['_pvc_default_screenshot'][0],
			'responsive_padding' => $metas['_hvc_video_responsive_padding'][0]
		);
	}
}
